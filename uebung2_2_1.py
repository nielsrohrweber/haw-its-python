#ITS HAW 15/10/13 Uebung 2.2.1
from math import *

def Zylinder(d,h):
	d=float(d)
	h=float(h)
	r=d/2.
	print "Zylinder mit d=%.2f und h=%.2f." %(d, h)
	print "Radius: ", r
	print "Volumen: ", pi*(r**r)*h
	print "Mantelflaeche: ", 2*pi*r*h
	print "Oberflaeche: ", 2*pi*r*h+2*pi*(r**r)
	
Zylinder(4,5)
