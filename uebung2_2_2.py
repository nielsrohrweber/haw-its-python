#ITS HAW 15/10/13 Uebung 2.2.2
from math import *
from turtle import *

def Sechseck (a, x0, y0):
    #reset()
    penup()
    goto(x0-a, y0)
    
    h2 = a**2-(a/2)**2
    h = sqrt (h2)
    
    pendown()
    goto(x0-a/2, y0-h)
    goto(x0+a/2, y0-h)
    goto(x0+a, y0)
    goto(x0+a/2, y0+h)
    goto(x0-a/2, y0+h)
    goto(x0-a, y0)

Sechseck (50, 0, 0)
Sechseck (50, 0, 200)
    
 
raw_input()
		